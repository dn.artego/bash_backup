#!/bin/bash
. ./config_file

# Создаем директорию для резервных копий
sudo mkdir $FOLDER_BACKUP

# Меняеем владельца папки на пользователя
sudo chown -R $USER:$USER $FOLDER_BACKUP

# Удаляем старый файл
rm ~/autoback.sh

# Копирование скрипта в домашнюю дерикторию
cat ./config_file ./autoback.sh >> ~/autoback.sh

# Установка времени выполнения скрипта, в файле time.txt находится тайминг периодичности выполнения скрипта
crontab ./time.txt
