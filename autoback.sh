# Архивация директорий и файлов в zip
zip -r _archive.zip ./rails ./ruby ./Документы ./Загрузки ./Шаблоны ./bash_script "./Рабочий стол"

# Создаем директорию для резервных копий
sudo mkdir $FOLDER_BACKUP

# Копирование файла в директория backup
rsync -azh --progress _archive.zip $FOLDER_BACKUP/home_`date +%Y-%m-%d_%H-%M`.zip

# Поиск старых архивов более определенного времени и удаление
find $FOLDER_BACKUP -mmin +3 -delete
